package com.example.flyhigh.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Aircraft {

    @Id
    @GeneratedValue
    private Long aircraftId;

    private String manufacturer;

    private String model;

    private Integer numberOfSeats;

    @OneToMany(mappedBy = "aircraft")
    private List<Flight> flights = new ArrayList<>();

    public Aircraft(Aircraft aircraft) {

    }
}

