package com.example.flyhigh.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Passenger {

    @Id
    @GeneratedValue()
    private Long passengerId;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String email;

    private String address;

    private String bankName;

    private long cardNumber;

    @DateTimeFormat(pattern = "MM/yy")
    private LocalDate expiryDate;

    private int cvvNumber;

    @OneToOne
    private Ticket ticket;

    public Passenger(Passenger passenger) {

    }
}
