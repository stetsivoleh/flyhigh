package com.example.flyhigh.domain;

public enum UserRole {
    ROLE_USER, ROLE_ADMIN
}
