package com.example.flyhigh.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Flight {

    @Id
    @GeneratedValue
    private Long flightId;

    private String flightNumber;

    @ManyToOne
    private Airport departureAirport;

    @ManyToOne
    private Airport destinationAirport;

    @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm")
    private LocalDate departureDate;

    @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm")
    private LocalDate arrivalDate;


    @ManyToOne
    private Aircraft aircraft;

    @OneToOne(mappedBy = "flight", fetch = FetchType.LAZY)
    private Ticket ticket;

}
