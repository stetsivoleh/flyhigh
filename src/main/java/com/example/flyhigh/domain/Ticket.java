package com.example.flyhigh.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Ticket {

    @Id
    @GeneratedValue
    private Long ticketId;

    private Integer row;

    private Integer seat;

    @ManyToOne
    private User user;

    @OneToOne(fetch = FetchType.LAZY)
    private Flight flight;

    @OneToOne(fetch = FetchType.LAZY)
    private Passenger passenger;

}
