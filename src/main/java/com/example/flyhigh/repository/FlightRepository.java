package com.example.flyhigh.repository;

import com.example.flyhigh.domain.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {

    @Query(value = "select f.* from flight f join airport a on f.departure_airport_airport_id = a.airport_id where a.airport_id =:id",
            nativeQuery = true)
    List<Flight> findAllByDepartureAirportId(@Param("id") Long id);

    @Query(value = "select f.* from flight f join airport a on f.destination_airport_airport_id = a.airport_id where a.airport_id =:id",
            nativeQuery = true)
    List<Flight> findAllByDestinationAirportId(@Param("id") Long id);

    @Query(value = "select f.* from flight f join aircraft a on f.aircraft_aircraft_id = a.aircraft_id where a.aircraft_id =:id",
            nativeQuery = true)
    List<Flight> findAllByAircraftId(@Param("id") Long id);
}
