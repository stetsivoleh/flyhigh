package com.example.flyhigh.repository;

import com.example.flyhigh.domain.Ticket;
import com.example.flyhigh.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long>{

    @Query(value = "select t.* from ticket t join user on ticket.user_id = user.id where user.id =:id",
    nativeQuery = true)
    List<Ticket> findAllByUserId(@Param("id") Long id);

    List<Ticket> findAll();


}
