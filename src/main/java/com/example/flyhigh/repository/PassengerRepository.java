package com.example.flyhigh.repository;

import com.example.flyhigh.domain.Flight;
import com.example.flyhigh.domain.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger,Long> {


}
