package com.example.flyhigh.dto.response;

import com.example.flyhigh.domain.Ticket;

public class TicketResponseDTO {

    private Long id;

    private Integer row;

    private Integer seat;

    public TicketResponseDTO(Ticket ticket) {
        this.id = ticket.getTicketId();
        this.row = ticket.getRow();
        this.seat = ticket.getSeat();
    }
}
