package com.example.flyhigh.dto.response;

import com.example.flyhigh.domain.Airport;
import com.example.flyhigh.domain.Flight;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class FlightResponseDTO {

    private Long flightId;

    private String flightNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm")
    private LocalDate departureDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm")
    private LocalDate arrivalDate;

    public FlightResponseDTO(Flight flight) {
        this.flightId = flight.getFlightId();
        this.flightNumber = flight.getFlightNumber();
        this.departureDate = flight.getDepartureDate();
        this.arrivalDate = flight.getArrivalDate();
    }
}
