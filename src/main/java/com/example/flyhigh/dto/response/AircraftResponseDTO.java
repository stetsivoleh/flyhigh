package com.example.flyhigh.dto.response;

import com.example.flyhigh.domain.Aircraft;

public class AircraftResponseDTO {

    private Long aircraftId;

    private String manufacturer;

    private String model;

    private Integer numberOfSeats;

    public AircraftResponseDTO(Aircraft aircraft) {
        this.aircraftId = aircraft.getAircraftId();
        this.manufacturer = aircraft.getManufacturer();
        this.model = aircraft.getModel();
        this.numberOfSeats = aircraft.getNumberOfSeats();
    }
}
