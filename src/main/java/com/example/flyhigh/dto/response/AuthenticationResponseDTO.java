package com.example.flyhigh.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticationResponseDTO {

	private Long id;

	private String username;

	private String token;

	public AuthenticationResponseDTO(String username, String token, Long id) {
		this.username = username;
		this.token = token;
		this.id = id;
	}

}
