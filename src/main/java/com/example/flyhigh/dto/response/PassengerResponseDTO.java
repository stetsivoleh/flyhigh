package com.example.flyhigh.dto.response;

import com.example.flyhigh.domain.Passenger;

public class PassengerResponseDTO {

    private Long passengerId;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String email;

    public PassengerResponseDTO(Passenger passenger) {
        this.passengerId = passenger.getPassengerId();
        this.firstName = passenger.getFirstName();
        this.lastName = passenger.getLastName();
        this.phoneNumber = passenger.getPhoneNumber();
        this.email = passenger.getEmail();
    }
}
