package com.example.flyhigh.dto.response;

import com.example.flyhigh.domain.Airport;

public class AirportResponseDTO {

    private Long airportId;

    private String airportCode;

    private String airportName;

    private String city;

    private String country;

    public AirportResponseDTO(Airport airport) {
        this.airportId = airport.getAirportId();
        this.airportCode = airport.getAirportCode();
        this.airportName = airport.getAirportName();
        this.city = airport.getCity();
        this.country = airport.getCountry();
    }
}
