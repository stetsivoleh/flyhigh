package com.example.flyhigh.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TicketRequestDTO {

    private Integer row;

    private Integer seat;

    private Long passengerId;
}
