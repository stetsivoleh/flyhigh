package com.example.flyhigh.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightRequestDTO {

    private String flightNumber;

    private Long ticketId;
}
