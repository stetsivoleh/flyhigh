package com.example.flyhigh.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirportRequestDTO {

    private String airportCode;
}
