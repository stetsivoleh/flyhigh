package com.example.flyhigh.service;

import com.example.flyhigh.domain.Flight;
import com.example.flyhigh.dto.request.FlightRequestDTO;
import com.example.flyhigh.repository.AircraftRepository;
import com.example.flyhigh.repository.AirportRepository;
import com.example.flyhigh.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightServiceImpl implements FlightService{

    private FlightRepository flightRepository;

    private AirportRepository airportRepository;

    private AircraftRepository aircraftRepository;

    @Autowired
    public FlightServiceImpl(FlightRepository flightRepository, AirportRepository airportRepository,
                             AircraftRepository aircraftRepository) {
        this.flightRepository = flightRepository;
        this.airportRepository = airportRepository;
        this.aircraftRepository = aircraftRepository;
    }

    @Override
    public void save(Flight flight) {
        flightRepository.save(flight);
    }

    @Override
    public void deleteById(Long id) {
        flightRepository.deleteById(id);
    }

    @Override
    public List<Flight> getAll() {
        return flightRepository.findAll();
    }

    @Override
    public List<Flight> getAllByDepartureAirportId(Long id) {
        return flightRepository.findAllByDepartureAirportId(airportRepository.getReferenceById(id).getAirportId());
    }

    @Override
    public List<Flight> getAllByDestinationAirportId(Long id) {
        return flightRepository.findAllByDestinationAirportId(airportRepository.getReferenceById(id).getAirportId());
    }

    @Override
    public List<Flight> getAllByAircraftId(Long id) {
        return flightRepository.findAllByAircraftId(aircraftRepository.getReferenceById(id).getAircraftId());
    }

    @Override
    public Flight getFlightById(long flightId) {
        return flightRepository.findById(flightId).orElse(null);
    }

    @Override
    public Flight update(FlightRequestDTO flightRequestDTO, Long id) {

        return flightRepository.save(mapFlightRequestToFlight(flightRequestDTO, getFlightById(id)));
    }

    private Flight mapFlightRequestToFlight(FlightRequestDTO flightRequestDTO, Flight flight) {

        if (flight == null){
            flight = new Flight();
        }
        flight.setFlightNumber(flightRequestDTO.getFlightNumber());

        return flight;
    }
}
