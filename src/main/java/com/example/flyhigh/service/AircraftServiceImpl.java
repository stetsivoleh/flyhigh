package com.example.flyhigh.service;

import com.example.flyhigh.domain.Aircraft;
import com.example.flyhigh.domain.Flight;
import com.example.flyhigh.dto.request.AircraftRequestDTO;
import com.example.flyhigh.dto.request.FlightRequestDTO;
import com.example.flyhigh.dto.response.AircraftResponseDTO;
import com.example.flyhigh.repository.AircraftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AircraftServiceImpl implements AircraftService{

    private AircraftRepository aircraftRepository;

    @Autowired
    public AircraftServiceImpl(AircraftRepository aircraftRepository) {
        this.aircraftRepository = aircraftRepository;
    }

    @Override
    public List<Aircraft> getAll() {
        return aircraftRepository.findAll();
    }

    @Override
    public Aircraft update(AircraftRequestDTO aircraft, Long id) {
        return aircraftRepository.save(mapAircraftRequestToAircraft(aircraft, getById(id)));
    }

    private Aircraft mapAircraftRequestToAircraft(AircraftRequestDTO aircraftRequestDTO, Aircraft aircraft) {

        if (aircraft == null){
            aircraft = new Aircraft();
        }
        aircraft.setModel(aircraftRequestDTO.getModel());

        return aircraft;
    }

    @Override
    public Aircraft getById(Long aircraftId) {
        return aircraftRepository.findById(aircraftId).orElse(null);
    }

    @Override
    public Aircraft save(Aircraft aircraft) {
        if(aircraft==null) throw new IllegalArgumentException();
        return aircraftRepository.save(aircraft);
    }

    @Override
    public void deleteById(Long aircraftId) {
        aircraftRepository.deleteById(aircraftId);
    }
}
