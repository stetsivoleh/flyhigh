package com.example.flyhigh.service;

import com.example.flyhigh.domain.Flight;
import com.example.flyhigh.domain.Passenger;
import com.example.flyhigh.dto.request.PassengerRequestDTO;
import com.example.flyhigh.dto.response.PassengerResponseDTO;

import java.util.List;

public interface PassengerService {

    List<Passenger> getAll();

    Passenger getById(Long passengerId);

    Passenger save(Passenger passenger);

    void deleteById(Long passengerId);

    Passenger update(PassengerRequestDTO passenger, Long id);
}
