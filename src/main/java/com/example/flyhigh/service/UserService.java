package com.example.flyhigh.service;

import com.example.flyhigh.domain.User;
import com.example.flyhigh.dto.request.UserLoginRequestDTO;
import com.example.flyhigh.dto.request.UserRegistrationRequestDTO;
import com.example.flyhigh.dto.response.AuthenticationResponseDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;


public interface UserService extends UserDetailsService {

    AuthenticationResponseDTO registerUser(UserRegistrationRequestDTO requestDTO);

    AuthenticationResponseDTO login(UserLoginRequestDTO requestDTO);

    User findByLogin(String username);

    Optional<User> getById(Long id);

    User getCurrentUser();
}
