package com.example.flyhigh.service;

import com.example.flyhigh.domain.Flight;
import com.example.flyhigh.dto.request.FlightRequestDTO;

import java.util.List;

public interface FlightService {

    void save(Flight flight);

    void deleteById(Long id);

    List<Flight> getAll();

    List<Flight> getAllByDepartureAirportId(Long id);

    List<Flight> getAllByDestinationAirportId(Long id);

    List<Flight> getAllByAircraftId(Long id);

    Flight getFlightById(long flightId);

    Flight update(FlightRequestDTO flightRequestDTO, Long id);
}
