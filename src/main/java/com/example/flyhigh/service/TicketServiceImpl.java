package com.example.flyhigh.service;

import com.example.flyhigh.domain.Flight;
import com.example.flyhigh.domain.Ticket;
import com.example.flyhigh.dto.request.FlightRequestDTO;
import com.example.flyhigh.dto.request.TicketRequestDTO;
import com.example.flyhigh.repository.PassengerRepository;
import com.example.flyhigh.repository.TicketRepository;
import com.example.flyhigh.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketServiceImpl implements TicketService{

    private TicketRepository ticketRepository;

    private UserService userService;

    private PassengerService passengerService;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository, UserService userService, PassengerService passengerService) {
        this.ticketRepository = ticketRepository;
        this.userService = userService;
        this.passengerService = passengerService;
    }


    @Override
    public List<Ticket> getAll() {
        return ticketRepository.findAll();
    }

    @Override
    public void save(Ticket ticket) { ticketRepository.save(ticket); }

    @Override
    public Ticket update(TicketRequestDTO ticketRequestDTO, Long id) {

        return ticketRepository.save(mapTicketRequestToTicket(ticketRequestDTO, ticketRepository.getReferenceById(id)));
    }

    private Ticket mapTicketRequestToTicket(TicketRequestDTO ticketRequestDTO, Ticket ticket) {

        if (ticket == null){
            ticket = new Ticket();
        }
        ticket.setRow(ticketRequestDTO.getRow());
        ticket.setSeat(ticketRequestDTO.getSeat());
        ticket.setPassenger(passengerService.getById(ticketRequestDTO.getPassengerId()));

        return ticket;
    }

    @Override
    public List<Ticket> getAllByUserId(Long userId) {
        return ticketRepository.findAllByUserId(userService.getById(userId).get().getId());
    }
}
