package com.example.flyhigh.service;

import com.example.flyhigh.domain.Aircraft;
import com.example.flyhigh.dto.request.AircraftRequestDTO;
import com.example.flyhigh.dto.response.AircraftResponseDTO;

import java.util.List;

public interface AircraftService {

    List<Aircraft> getAll();

    Aircraft update(AircraftRequestDTO aircraft, Long id);

    Aircraft getById(Long aircraftId);

    Aircraft save(Aircraft aircraft);

    void deleteById(Long aircraftId);
}
