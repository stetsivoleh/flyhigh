package com.example.flyhigh.service;

import com.example.flyhigh.domain.Airport;

import java.util.List;

public interface AirportService {

    List<Airport> getAll();

    Airport getById(Long airportId);

    Airport save(Airport airport);

    void deleteById(Long airportId);
}
