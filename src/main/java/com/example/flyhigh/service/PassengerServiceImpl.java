package com.example.flyhigh.service;

import com.example.flyhigh.domain.Passenger;
import com.example.flyhigh.domain.Ticket;
import com.example.flyhigh.dto.request.PassengerRequestDTO;
import com.example.flyhigh.dto.request.TicketRequestDTO;
import com.example.flyhigh.dto.response.PassengerResponseDTO;
import com.example.flyhigh.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService{

    private PassengerRepository passengerRepository;

    @Override
    public Passenger update(PassengerRequestDTO passenger, Long id) {
        return passengerRepository.save(mapPassengerRequestToPassenger(passenger, getById(id)));
    }

    private Passenger mapPassengerRequestToPassenger(PassengerRequestDTO passengerRequestDTO, Passenger passenger) {

        if (passenger == null){
            passenger = new Passenger();
        }
        passenger.setFirstName(passengerRequestDTO.getFirstName());
        passenger.setLastName(passengerRequestDTO.getLastName());

        return passenger;
    }

    @Autowired
    public PassengerServiceImpl(PassengerRepository passengerRepository) {
        this.passengerRepository = passengerRepository;
    }

    @Override
    public List<Passenger> getAll() {
        return passengerRepository.findAll();
    }

    @Override
    public Passenger getById(Long passengerId) {
        return passengerRepository.findById(passengerId).orElse(null);
    }

    @Override
    public Passenger save(Passenger passenger) {
        return passengerRepository.save(passenger);
    }

    @Override
    public void deleteById(Long passengerId) {
        passengerRepository.deleteById(passengerId);
    }
}
