package com.example.flyhigh.service;

import com.example.flyhigh.domain.Flight;
import com.example.flyhigh.domain.Ticket;
import com.example.flyhigh.dto.request.FlightRequestDTO;
import com.example.flyhigh.dto.request.TicketRequestDTO;

import java.util.List;

public interface TicketService {

    List<Ticket> getAllByUserId(Long userId);

    List<Ticket> getAll();

    void save(Ticket ticket);

    Ticket update(TicketRequestDTO ticketRequestDTO, Long id);
}
