package com.example.flyhigh.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

// Base Configuration for enabling global method security.
// Classes may extend this class to customize the defaults,
// but must be sure to specify the EnableGlobalMethodSecurity annotation on the subclass.
@Configuration
@EnableGlobalMethodSecurity(
		prePostEnabled = true
)
public class SecurityMethodsConfiguration extends GlobalMethodSecurityConfiguration {

}
