package com.example.flyhigh.controller;

import com.example.flyhigh.dto.request.UserLoginRequestDTO;
import com.example.flyhigh.dto.request.UserRegistrationRequestDTO;
import com.example.flyhigh.dto.response.AuthenticationResponseDTO;
import com.example.flyhigh.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private UserServiceImpl userServiceImpl;

    @Autowired
    public UserController(UserServiceImpl userService) {
        this.userServiceImpl = userService;
    }

    @PostMapping("/register")
    public AuthenticationResponseDTO registerUser(@RequestBody UserRegistrationRequestDTO requestDTO) {
        return userServiceImpl.registerUser(requestDTO);
    }

    @PostMapping("/login")
    public AuthenticationResponseDTO login(@RequestBody UserLoginRequestDTO requestDTO) {
        return userServiceImpl.login(requestDTO);
    }
}
