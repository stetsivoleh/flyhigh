package com.example.flyhigh.controller;

import com.example.flyhigh.domain.Airport;
import com.example.flyhigh.dto.response.AirportResponseDTO;
import com.example.flyhigh.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/airports")
public class AirportController {

    private AirportService airportService;

    @Autowired
    public AirportController(AirportService airportService) {
        this.airportService = airportService;
    }

    @PostMapping
    private void createAirport(@RequestBody Airport airport) throws IOException { airportService.save(airport); }

    @GetMapping
    private List<AirportResponseDTO> getAll() {
        return airportService.getAll().stream()
                .map(AirportResponseDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    private AirportResponseDTO getById(@PathVariable("id") Long id) {
        return new AirportResponseDTO(airportService.getById(id));
    }

    @DeleteMapping("/{id}")
    private void deleteAirport(@PathVariable("id") Long id) {
        airportService.deleteById(id);
    }
}
