package com.example.flyhigh.controller;

import com.example.flyhigh.domain.Ticket;
import com.example.flyhigh.dto.request.TicketRequestDTO;
import com.example.flyhigh.dto.response.TicketResponseDTO;
import com.example.flyhigh.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    private TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping("/user/{userId}")
    private List<TicketResponseDTO> getAllByUserId(@PathVariable("userId") Long id) {
        return ticketService.getAllByUserId(id)
                .stream()
                .map(TicketResponseDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping
    private List<TicketResponseDTO> getAll() {
        return ticketService.getAll()
                .stream()
                .map(TicketResponseDTO::new)
                .collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    private TicketResponseDTO update(@RequestBody TicketRequestDTO request, @PathVariable("id") Long id) {
        return new TicketResponseDTO((ticketService.update(request, id)));
    }

    @PostMapping
    private void createTicket(@RequestBody Ticket ticket) {
        ticketService.save(ticket);
    }
}
