package com.example.flyhigh.controller;

import com.example.flyhigh.domain.Aircraft;
import com.example.flyhigh.dto.request.AircraftRequestDTO;
import com.example.flyhigh.dto.response.AircraftResponseDTO;
import com.example.flyhigh.service.AircraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/aircrafts")
public class AircraftController {

    private AircraftService aircraftService;

    @Autowired
    public AircraftController(AircraftService aircraftService) {
        this.aircraftService = aircraftService;
    }

    @PostMapping
    private void createAircraft(@RequestBody Aircraft aircraft) throws IOException {
        aircraftService.save(aircraft);
    }

    @PutMapping("/{id}")
    private AircraftResponseDTO update(@RequestBody AircraftRequestDTO aircraftRequestDTO, @PathVariable("id") Long id) {
       return new AircraftResponseDTO(aircraftService.update(aircraftRequestDTO, id));
    }

    @GetMapping
    private List<AircraftResponseDTO> getAll() {
        return aircraftService.getAll().stream()
                .map(AircraftResponseDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    private AircraftResponseDTO getById(@PathVariable("id") Long id) {
        return new AircraftResponseDTO(aircraftService.getById(id));
    }

    @DeleteMapping("/{id}")
    private void deleteAircraft(@PathVariable("id") Long id) {
        aircraftService.deleteById(id);
    }
}
