package com.example.flyhigh.controller;

import com.example.flyhigh.domain.Airport;
import com.example.flyhigh.domain.Flight;
import com.example.flyhigh.dto.request.FlightRequestDTO;
import com.example.flyhigh.dto.response.FlightResponseDTO;
import com.example.flyhigh.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/flights")
public class FlightController {

    private FlightService flightService;

    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping
    private List<FlightResponseDTO> getAll() {
        return flightService.getAll()
                .stream()
                .map(FlightResponseDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/airport/departure/{id}")
    private List<FlightResponseDTO> getAllByDepartureAirportId(@PathVariable("id") Long id) {
        return flightService.getAllByDepartureAirportId(id)
                .stream()
                .map(FlightResponseDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/airport/destination/{id}")
    private List<FlightResponseDTO> getAllByDestinationAirportId(@PathVariable("id") Long id) {
        return flightService.getAllByDestinationAirportId(id)
                .stream()
                .map(FlightResponseDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/aircraft/{id}")
    private List<FlightResponseDTO> getAllByAircraftId(@PathVariable("id") Long id) {
        return flightService.getAllByAircraftId(id)
                .stream()
                .map(FlightResponseDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping
    private void createFlight(@RequestBody Flight flight) throws IOException {
        flightService.save(flight);
    }

    @PutMapping("/{id}")
    private FlightResponseDTO updateFlight(@RequestBody FlightRequestDTO requestDTO, @PathVariable("id") Long id) throws IOException {
        return new FlightResponseDTO(flightService.update(requestDTO, id));
    }

    @DeleteMapping("/{id}")
    private void deleteItem(@PathVariable("id") Long id) {
        flightService.deleteById(id);
    }
}
