package com.example.flyhigh.controller;

import com.example.flyhigh.domain.Airport;
import com.example.flyhigh.domain.Passenger;
import com.example.flyhigh.dto.request.PassengerRequestDTO;
import com.example.flyhigh.dto.response.PassengerResponseDTO;
import com.example.flyhigh.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/passengers")
public class PassengerController {

    private PassengerService passengerService;

    @Autowired
    public PassengerController(PassengerService passengerService) {
        this.passengerService = passengerService;
    }

    @PostMapping
    private void create(@RequestBody Passenger passenger) throws IOException {
        passengerService.save(passenger);
    }

    @PutMapping("/{id}")
    private PassengerResponseDTO update(@RequestBody PassengerRequestDTO passenger, @PathVariable("id") Long id) throws IOException {
        return new PassengerResponseDTO(passengerService.update(passenger, id));
    }

    @GetMapping
    private List<PassengerResponseDTO> getAll() {
        return passengerService.getAll()
            .stream()
            .map(PassengerResponseDTO::new)
            .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    private PassengerResponseDTO getById(@PathVariable("id") Long id) {
        return new PassengerResponseDTO(passengerService.getById(id));
    }

    @DeleteMapping("/{id}")
    private void deleteAirport(@PathVariable("id") Long id) {
        passengerService.deleteById(id);
    }
}
